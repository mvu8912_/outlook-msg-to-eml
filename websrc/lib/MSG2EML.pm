use Dancer ":syntax";
use Email::Outlook::Message;

get "/" => sub {
    template "index";
};

post "/" => sub {
    my $io   = var "file";
    my $eml  = eval { Email::Outlook::Message->new($io)->to_email_mime }
      or halt "Parsing MSG file error: $@";

    if ( param("type") eq "download" ) {
        content_type "message/rfc822";
        header "Content-Disposition" => "attachment; filename=converted.eml";
    }
    else {
        content_type "text/plain";
    }
    return $eml->as_string;
};

hook before => sub {
    if ( request->method eq "GET" ) {
        return;
    }

    if ( request->content_type =~ /^multipart/ ) {
        my ( $field, $upload_obj ) = %{ request->uploads };
        my $fh = $upload_obj->file_handle;
        binmode $fh, "utf8";
        return var(file => $fh);
    }

    halt "No upload file";
};

1;
