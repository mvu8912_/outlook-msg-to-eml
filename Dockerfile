FROM resumelibrary/perl-php-base:with_plenv

MAINTAINER Michael Vu <email@michael.vu>

ADD websrc /websrc
RUN cd /websrc; /root/.plenv/bin/plenv global 5.20.2
RUN cd /websrc; /root/.plenv/bin/plenv exec dzil build; /root/.plenv/bin/plenv exec cpanm -L local --installdeps --notest MSG2EML-0.01.tar.gz; rm -fr /root/.cpanm
RUN cd /websrc; /root/.plenv/bin/plenv exec cpanm local::lib Starman --notest; rm -fr /root/.cpanm

EXPOSE 5000

CMD cd /websrc; /root/.plenv/bin/plenv exec perl -Mlocal::lib=local -I lib -- local/bin/plackup -s Starman --port 5000 bin/app.pl
